#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  int ietsprom=syscall(SYS_open, "prompte", O_RDONLY, 0755);
  char letter[1];
  std::string prompt ="";
  while(syscall(SYS_read, ietsprom, letter, 1))
   prompt += letter;

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{
  std::string bestandsnaam;
  std::string tekst;
  std::string filetekst;
 
  std::cout << "Vul de bestands naam in:"; 
  std::getline(std::cin, bestandsnaam);
  std::cout <<"Vul hier een tekst in:";
  while (std::getline(std::cin,tekst)){
    if (tekst=="<EOF>"){
      break;
    }
    filetekst+=tekst+"\n";
  }
const char* stdstringfiletekst[] = {filetekst.c_str()};
const char* stdstringbestandnaam[]= {bestandsnaam.c_str()};
int fd = syscall(SYS_creat, stdstringbestandnaam[0], 0755);
syscall(SYS_write, fd, stdstringfiletekst[0], filetekst.size());
}

void list() // ToDo: Implementeer volgens specificatie.
{
const char* argumenten[]={"/bin/ls","-la",NULL};
if(syscall(SYS_fork)==0){
  syscall(SYS_execve,argumenten[0],argumenten,NULL);
}else{
syscall(SYS_wait4, 0,NULL,NULL,NULL );
}
}

void find() // ToDo: Implementeer volgens specificatie.
{ std::string zoekwoord; 
  std::cout<<"Welk woord wil je vinden????";
  std::getline(std::cin, zoekwoord);

  const char* cstringzoekwoord= zoekwoord.c_str();

  int fd[2];

  syscall(SYS_pipe, &fd);

  int pid_a= syscall(SYS_fork);
  if(pid_a==0){
    const char* argumenten[]={"/bin/find",".",NULL};
    syscall(SYS_close, fd[0]);
    syscall(SYS_dup2, fd[1], 1);
    syscall(SYS_execve,argumenten[0],argumenten,NULL);}
    else{
     int pid_b= syscall(SYS_fork);
  if(pid_b==0){
    const char* argumenten1[]={"/bin/grep",cstringzoekwoord ,NULL};
    syscall(SYS_close, fd[1]);
    syscall(SYS_dup2, fd[0], 0);
    syscall(SYS_execve,argumenten1[0],argumenten1,NULL);
  }else{
    syscall(SYS_close, fd[0]);
    syscall(SYS_close, fd[1]);
    syscall(SYS_wait4, pid_a,NULL,NULL,NULL );
    syscall(SYS_wait4, pid_b,NULL,NULL,NULL );
    } 

  }

}

void seek() // ToDo: Implementeer volgens specificatie.
{ 
  std::string bestand2="seek";
  std::string inhoudbestand2="x";

  const char* cstringbestandnaam2[]= {bestand2.c_str()};
  const char* cstringinhoud2[]= {inhoudbestand2.c_str()};

  int makeseek=syscall(SYS_creat, cstringbestandnaam2[0], 0755);
  syscall(SYS_write, makeseek, cstringinhoud2[0], inhoudbestand2.size());
  int inhoudseek= syscall(SYS_lseek,makeseek,5000001,SEEK_SET);
  syscall(SYS_write, makeseek, inhoudseek, inhoudbestand2.size());
  syscall(SYS_write, makeseek, cstringinhoud2[0], inhoudbestand2.size());
  
  
  std::string bestand1="loop";
  std::string inhoudbestand1="x";
  for (unsigned int i=0; i<5000000; i++){
    inhoudbestand1+='\0';
  }
  inhoudbestand1+='x';

  const char* cstringbestandnaam[]= {bestand1.c_str()};
  const char* cstringinhoud1[]= {inhoudbestand1.c_str()};
  int makeloop=syscall(SYS_creat, cstringbestandnaam[0], 0755);
  syscall(SYS_write, makeloop, cstringinhoud1[0], inhoudbestand1.size());


 }

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
